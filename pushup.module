<?php
/**
 * @file
 * Provides API integration with the PushUp Notificaiton Service
 */

define ('PUSHUP_HOST',         'http:/pushupnotifications.com');
define ('PUSHUP_API',          'https://push.10up.com/json.php');

/**
 * Implements hook_menu().
 */
function pushup_menu() {
  $items['admin/config/services/pushup'] = array(
    'title' => 'PushUp',
    'description' => 'PushUp Settings.',
    'page callback' => 'pushup_user_settings',
    'access arguments' => array('administer site configuration'),
    'file' => 'pushup.pages.inc',
  );
  $items['admin/config/services/pushup/default'] = array(
    'title' => 'PushUp',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/config/services/pushup/settings'] = array(
    'title' => 'Settings',
    'description' => 'PushUp settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pushup_admin_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'pushup.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/services/pushup/analytics'] = array(
    'title' => 'Analytics',
    'description' => 'Information about PushUp Subscribers',
    'page callback' => 'pushup_analytics',
    'access arguments' => array('administer site configuration'),
    'file' => 'pushup.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 50,
  );
  $items['admin/config/services/pushup/notifications'] = array(
    'title' => 'Notification Display',
    'description' => 'Customize your notification title and icon',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pushup_notifications_admin_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'pushup.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 50,
  );
  return $items;
}

/**
 * Implements hook_init
 */
function pushup_init() {
   global $base_url;

   if ($cache = cache_get('pushup_domain_config')) {
       $result = $cache->data;
       $website_pushID = $result->push_id;
   } else {
     $result = pushup_get_settings_data('','');
     $website_pushID = $result->{'push.domain.config'}->push_id;
   }
   if($cache = cache_get('pushup_userid')) {
     $pushup_userid = $result;
   } else {
     $result = pushup_get_settings_data('','');
     $pushup_userid = $result->{'push.user.id'};
   }
   $pushup_settings = array(
     'domain' => $base_url,
     'userID' => $pushup_userid,
     'websitePushID' => $website_pushID,
     'webServiceURL' => PUSHUP_API,
   );
   drupal_add_js(array('pushup' => $pushup_settings),'setting');

   drupal_add_js(drupal_get_Path('module','pushup') . '/js/pushup.js',array('scope'=> 'footer',));
}

/*
 * Implements hook theme, for image upload form
 */
function pushup_theme() {
  return array(
    'pushup_thumb_upload' => array(
      'render element' => 'element',
      'file' => 'pushup.pages.inc',
  ));
}

/**
 * Implements hook_form_alter().
 */
function pushup_form_alter(&$form, $form_state, $form_id) {
    // After that a checkbox for settings.
  if (!empty($form['#node_edit_form'])) {
    $form['pushup'] = array(
      '#type'=> 'fieldset',
      '#title' => t('Push Notifications'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#group' => 'additional_settings',
      '#attached' => array(
        'js' => array(
          'pushup' => drupal_get_path('module','pushup') . '/js/pushup_vertical_tabs.js',
        )
      )
    );
    $node = $form['#node'];
    $push_status = '';
    if($node && !empty($node->nid)) {
      $push_status = db_select('pushup','push_status')
      ->fields('push_status')
      ->condition('id', $node->nid, '=')
      ->execute();
    }
    if($push_status && $push_status->rowCount() != 0) {
      $push_status = $push_status->fetchField(1);
      if($push_status == '1' || $push_status == '0') {
        $form['pushup']['enabled'] = array(
          '#type' => 'checkbox',
          '#title' => t('Publish a push notification when published.'),
          '#default' => FALSE,
        );
        if($push_status == 1 ) {
          $form['pushup']['enabled']['#attributes']['checked']='checked';
        }
      } else {
        // in this case push status was not zero or 1 but is set - timestamp
        $form['pushup']['data'] = array(
          '#type' => 'item',
          '#description' => 'A push notification was sent for this item on '. format_date($push_status,'custom','l, F j \a\t g:ia'),
        );
      }
    } else {
      // no info was found
      $form['pushup']['enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Publish a push notification when published.'),
        '#default' => FALSE,
      );
    }
    $form['pushup']['pushup_container'] = array(
      '#type' => 'container',
      '#parents' => array('pushup'),
      '#states' => array(
        '#invisible' => array(
          'input[name="pushup[enabled]"]' => array('checked' => FALSE)
        )
      )
    );
  }
}


/**
 * Check the status of the pushup API
 * Inspired by Mollom's function for status of its API
 *
 * @return array
 * An associative array describing the current status:
 *  - hasNetwork: Boolean describing wheter we can ping the API
 *  - hasAPIKey: Boolean whether API key is present
 *  - hasUsername: Boolean whether username is present
 *  - isVerified: Boolean whether keys ahve been verified
 *  - response: The response code from the verification attempt
 */
function _pushup_status() {
  $cid = 'pushup_status';
  $expire_valid = 86400; // once per day
  $expire_invalid = 3600; // once per hour

  if ($cache = cache_get($cid, 'cache')) {
    if ($cache->expire > REQUEST_TIME) {
      $status = $cache->data;
      return $status;
    }
  }


  $status = array(
    'hasNetwork' => FALSE,
    'hasAPIKey' => FALSE,
    'hasUsername' => FALSE,
    'isVerified' => FALSE,
    'response' => NULL,
  );

  $status['hasAPIKey'] = variable_get('pushup_api_key', FALSE);
  $status['hasUsername'] = variable_get('pushup_username', FALSE);

  $data = array(
      'actions' => array(
        'push.authenticate' => array('username' => $status['hasUsername'],'api_key'  => $status['hasAPIKey'],)
      )
  );

  $request = array(
    'method' => 'POST', // HTTP Request Type
    'data' => json_encode($data), // Parameters
    'sslverify' => FALSE,
  );

  $dhr = drupal_http_request(PUSHUP_API,$request);

  if($dhr->code == '200') {
    $status['hasNetwork'] = TRUE;
  } else {
    $status['hasNetwork'] = FALSE;
  }

  $response = json_decode($dhr->data);

  if($response->{'push.authenticate'} == 'true') {
    $status['isVerified'] = TRUE;
  } else {
    $status['isVerified'] = FALSE;
    $status['response'] = $response;
  }
	$settings_data = pushup_get_settings_data();
		if ($settings_data->{'push.domain'}->enabled == 1) {
			$status['activated'] = TRUE;
	  } else {
			$status['activated'] = FALSE;
		}

  return $status;
}

/**
   * Gets the settings data for an ideal settings page with everything unlocked. Caches these settings to an internal
   * property so we don't make duplicate calls during the lifetime of a request. Note: we can cache this way simply
   * because the domain name will not change and we won't need to bust the cache during a page load.
   *
   * @return array|bool|mixed|null
   */
function pushup_get_settings_data( $username = '', $api_key = '' ) {

    // Use username if none passed
    if ( empty( $username ) ) {
      $username = variable_get('pushup_username', FALSE);
    }

    // Use api key is none passed
    if ( empty( $api_key ) ) {
      $api_key = variable_get('pushup_api_key', FALSE);
    }
    global $base_url;
    $site_url = $base_url;
    $actions  = array(
      'push.authenticate' => array(
        'username' => $username,
        'api_key'  => $api_key,
      ),
      'push.domain' => array(
        'domain' => $site_url,
      ),
      'push.analytics' => array(
        'domain' => $site_url,
      ),
      'push.domain.config' => array(
        'domain' => $site_url,
      ),
      'push.user.id' => array(
        'username' => $username,
        'api_key' => $api_key,
      )
    );

    $pushup_settings = pushup_perform_authenticated_query( $actions, $username, $api_key );

    // let's save the settings we have to cache, so we don't need to do this again
    $expires = time() + (60 * 60 * 6); // 6 hrs
    cache_set('pushup_userid',$pushup_settings->{'push.user.id'},'cache',$expires);
    cache_set('pushup_push_id',$pushup_settings->{'push.domain'}->push_id,'cache',$expires);
    cache_set('pushup_analytics',$pushup_settings->{'push.analytics'},'cache', $expires);
    cache_set('pushup_domain_config',$pushup_settings->{'push.domain.config'},'cache', $expires);

    return $pushup_settings;
  }

/**
   * Sends a list of actions to the API to be performed as needed.
   *
   * @param array $actions
   * @return array|bool|mixed
   */
function pushup_perform_authenticated_query( $actions = array(), $username = '', $api_key = '' ) {

    global $base_url;
    $site_url = $base_url;

    // Use username if none passed
    if ( empty( $username ) ) {
      $username = variable_get('pushup_username', FALSE);
    }

    // Use api key is none passed
    if ( empty( $api_key ) ) {
      $api_key = variable_get('pushup_api_key', FALSE);
    }

    if(empty($actions)) {
      $data = array(
        'actions' => array(
           'push.authenticate' => array('username' => $username,'api_key'  => $api_key),
           'push.domain' => array('domain' => $site_url,),
           'push.analytics' => array('domain' => $site_url,),
           'push.domain.config' => array('domain' => $site_url,),
           'push.user.id' => array('username' => $username,'api_key' => $api_key,)
         ),
         'username' => $username,
         'api_key'  => $api_key,
      );
    } else {
      $data = array('actions'=> $actions,'username' => $username,'api_key'  => $api_key,);
    }

    $request = array(
      'method' => 'POST',
      'data' => json_encode($data),
    );

    $response = drupal_http_request (PUSHUP_API, $request );
    //echo print_r($response,true);
    return json_decode($response->data);
}

/**
 * Implements hook_node_delete().
 */
function pushup_node_delete($node) {
  // On deleting a node, update the pushup table.
  db_delete('pushup')
    ->condition('id', $node->nid)
    ->execute();
}

/**
 * Implements hook_node_insert().
 *
 * As a new node is being inserted into the database, we need to do our own
 * database inserts.
 */
function pushup_node_insert($node) {
    if(!empty($node->status) && $node->pushup['enabled'] == 1) {
      // node is published AND push was requested
      // title = notification title, body = node title, action = 'read more', url argument = url without prefix
      $url_arguments = str_replace( array( 'http://', 'https://' ), '', url('node/' . $node->nid, array('absolute' => TRUE)) );
      $title = variable_get('pushup_post_title','Just published . . . ');
      $result = pushup_send_message( $title, $node->title,'Read More', $url_arguments);
      if(isset($result['status']) && $result['status'] == 'pushed') {
        db_insert('pushup')
          ->fields(array('push_status' => REQUEST_TIME,'id'=> $node->nid,))
          ->execute();
      }
    } else {
    db_insert('pushup')
    ->fields(array(
        'id' => $node->nid,
        'push_status' => $node->pushup['enabled'],
    ))
    ->execute();
  }
}

/**
 * Implements hook_node_update().
 *
 * As an existing node is being updated in the database, we need to do our own
 * database updates.
 *
 * This hook is called when an existing node has been changed. We can't simply
 * update, since the node may not have a rating saved, thus no
 * database field. So we first check the database for a rating. If there is one,
 * we update it. Otherwise, we call pushup_node_insert() to create one.
 */
function pushup_node_update($node) {
    //drupal_set_message('node_update '. print_r($node,true));
    $current_push_status = db_select('pushup','push_status')
      ->fields('push_status')
      ->condition('id', $node->nid, '=')
      ->execute();
    if ($current_push_status->rowCount() > 0) {
      //drupal_set_message('in node update, current status is '. $current_push_status->fetchField(1));
      if(isset($node->pushup['enabled'])) {
        //drupal_set_message('and what I am about to save is '. $node->pushup['enabled']);
        db_update('pushup')
          ->fields(array('push_status' => $node->pushup['enabled']))
          ->condition('id', $node->nid)
          ->execute();
      }
    } else {
      // Node was not previously rated, so insert a new rating in database.
      pushup_node_insert($node);
    }
    if(!empty($node->status) && isset($node->pushup['enabled']) && $node->pushup['enabled'] == 1) {
        // node is published AND push was requested
        // title = notification title, body = node title, action = 'read more', url argument = url without prefix
        $url_arguments = str_replace( array( 'http://', 'https://' ), '', url('node/' . $node->nid, array('absolute' => TRUE)) );
        $title = variable_get('pushup_post_title','Just published . . . ');
        $result = pushup_send_message( $title, $node->title,'Read More', $url_arguments);
        if(isset($result['status']) && $result['status'] == 'pushed') {
          db_update('pushup')
            ->fields(array('push_status' => REQUEST_TIME))
            ->condition('id', $node->nid)
            ->execute();
        }
    }
}


/**
 * This function is responsible for pushing the notification details to the
 * push notification server which will then validate the API request before
 * sending it.
 *
 * @param string $title
 * @param string $body
 * @param string $action
 * @param array $url_arguments
 * @return bool
 */
function pushup_send_message( $title = '', $body = '', $action = '', $url_arguments = array() ) {
  global $base_url;
  $username = variable_get('pushup_username', FALSE);
	$api_key  = variable_get('pushup_api_key',FALSE);

	if ( empty( $username ) || empty( $api_key ) ) {
		return false;
	}
  $settings_data = pushup_get_settings_data();
  if ($settings_data->{'push.domain'}->enabled != 1) {
    return false;
  }


  $post_data = array(
    'title'        => $title,
    'body'         => $body,
    'action'       => 'Read More',
    'username'     => $username,
    'api_key'      => $api_key,
    'url_arguments'=> array('0' => $url_arguments),
    'mode'         => 'all',
    'domain'       => $base_url,
  );


	$params = array(
    'headers'    => array( 'Content-Type' => 'application/x-www-form-urlencoded' ),
		'method'    => 'POST',
		'timeout'   => 12,
    'sslverify' => FALSE,
		'data'      => drupal_http_build_query($post_data),
	);

  $api_url = 'https://push.10up.com/send';

	// Default return value
	$retval = array(
		'time'   => REQUEST_TIME,
		'status' => 'error'
	);

	// Attempt to make the remote request
	$result = drupal_http_request( $api_url,$params);

	if (isset($result->error) ) {
    return $retval;
	}

	// Parse the body of the json request
	$body = $result->data;
	$data = @json_decode( $body, true );
	if ( ! is_array( $data ) ) {
		return $retval;
	}

  // Set the status to 'pushed'
	if ( isset( $data[ 'status' ] ) && $data[ 'status' ] === 'ok' ) {
		$retval['status'] = 'pushed';
	}

	// Return the return value
	return $retval;
}

/**
 * Implementation of hook_scheduler_api.
 *
 * @param $node
 *  The node object
 * @param $action
 *  The action being performed, either "publish" or "unpublish"
 */
function pushup_scheduler_api($node, $action) {
  $current_push_status = db_select('pushup','push_status')
    ->fields('push_status')
    ->condition('id', $node->nid, '=')
    ->execute();
  if($current_push_status->rowCount() > 0) {
    $push_status = $current_push_status->fetchField(1);
  }
  if ($action == 'publish' && $push_status == 1) {
    $url_arguments = str_replace( array( 'http://', 'https://' ), '', url('node/' . $node->nid, array('absolute' => TRUE)) );
    $title = variable_get('pushup_post_title','Just published . . . ');
    $result = pushup_send_message( $title, $node->title,'Read More', $url_arguments);
    if(isset($result['status']) && $result['status'] == 'pushed') {
      db_update('pushup')
        ->fields(array('push_status' => REQUEST_TIME))
        ->condition('id', $node->nid)
        ->execute();
      }
  }
}
