<?php
/**
 * @file
 * Page callbacks for PushUp module.
 */

/**
 * Pushup Notifications Status Area
 */
 function pushup_user_settings() {
  drupal_add_css(drupal_get_path('module', 'pushup') .'/css/settings.css');
  $output = '<h2>Push Up Status</h2>';

  $status = _pushup_status();

  if(!$status['hasAPIKey']) {
    $output .= '<p>You have NOT entered an API key</p>';
    return $output;
  }
  if(!$status['hasUsername']) {
    $output .= '<p>You have NOT entered a username</p>';
    return $output;
  }

  $output .= '<h3>Service Connectivity</h3>';
  if($status['hasNetwork']) {
    $output .= '<p class="pushup-connection-status status-success">The PushUp API was contacted successfully.</p>';
  } else {
    $output .= '<p class="pushup-connection-status status-failure">The PushUp API was NOT contacted successfully.</p>';
  }


  if($status['isVerified'] == '1') {
    $output .= '<p class="pushup-authentication-status status-success">Your PushUp username and API key are valid.</p>';
  } else {
    $output .= '<p class="pushup-authentication-status status-failure">Your PushUp username and API key could not be validated.</p>';
  }

  if($status['activated']) {
    $output .= '<p class="pushup-activation-status status-success">Pushup has been provisioned for your domain</p>';
  } else
    $output .= '<p class="pushup-activation-status status-failure">Pushup has NOT been provisioned for your domain</p>';

   return $output;
 }

/**
 * Pushup Notifications Status Area
 */
 function pushup_analytics() {
  drupal_add_css(drupal_get_path('module', 'pushup') .'/css/settings.css');
  drupal_add_js(drupal_get_path('module', 'pushup') .'/js/chart.js');
  $username = variable_get('pushup_username', FALSE);
  $api_key = variable_get('pushup_api_key', FALSE);
  $output = '';
  if ($cache = cache_get('pushup_analytics')) {
      $result = $cache->data;
      $analytics = $result;
  } else {
    echo '<p>Getting new analytics</p>';
    $result = pushup_get_settings_data($username,$api_key);
    echo print_r($result,true);
    $analytics = $result->{'push.analytics'};
  }

  $output .= '<h3>Analytics</h3>';
  $output .= '<script type="text/javascript">
			var pushup_chart_options = {
				segmentShowStroke : true,
				segmentStrokeColor : "rgba(0,0,0,.1)",
				segmentStrokeWidth : 2,
				animation : true,
				animationSteps : 120,
				animationEasing : "easeOutQuart",
				animateRotate : true,
				animateScale : false
			};
		</script>';
  $output .= '<p>Some quick statistics about your audience</p>';
  $output .= '<table class="form-table"><tbody>';
  if($analytics->total_prompted && $analytics->total_granted) {
    $output .= '<tr><th scope="row">Subscribers</th><td>';
    $output .= '<div class="pushup-analytics"><canvas id="pushup-chart-conversion" width="100" height="100" style="width: 100px; height: 100px;"></canvas><div class="description">';
    $output .= '<p class="pushup Declined">'. $analytics->total_declined . ' Declined</p>';
    $output .= '<p class="pushup Accepted">'. $analytics->total_granted .' Accepted</p></div><p class="description">Number of Subscribers</p>';
    $output .= '
        <script type="text/javascript">
          new Chart( jQuery( document.querySelector( "#pushup-chart-conversion" ) ).get(0).getContext( "2d" ) ).Pie( [
                    {
              color : "#63bde4",
              value : '. $analytics->total_declined .'					},									{
              color : "rgba(74,198,143,1)",
              value : '. $analytics->total_granted .'					}								], pushup_chart_options );
        </script>
      </div>

    </td></tr>';
  }

  $output .= '<tr><th scope="row">Push Requests</th><td>
      <div class="pushup-analytics">
        <canvas id="pushup-chart-requests" width="100" height="100" style="width: 100px; height: 100px;"></canvas>
        <div class="description">';
  $output .= '<p class="pushup ThisMonth">'. $analytics->total_monthly_pushes->total_push_requests .' This Month</p>';
  $output .= '<p class="pushup AllTime">'. $analytics->total_all_time_pushes->total_push_requests .' All Time</p>';
  $output .= '</div><p class="description">Total Number of Posted Items Pushed to Subscribers</p>';
  $output .= '
        <script type="text/javascript">
          new Chart( jQuery( document.querySelector( "#pushup-chart-requests" ) ).get(0).getContext( "2d" ) ).Pie( [
                    {
              color : "#63bde4",
              value : '. $analytics->total_monthly_pushes->total_push_requests .'		      },									{
              color : "rgba(74,198,143,1)",
              value : '. $analytics->total_all_time_pushes->total_push_requests .'					}								], pushup_chart_options );
        </script>
      </div>

    </td></tr>';
  $output .= '<tr><th scope="row">Notifications</th><td>
    <div class="pushup-analytics">
      <canvas id="pushup-chart-recipients" width="100" height="100" style="width: 100px; height: 100px;"></canvas>
      <div class="description">';
  $output .= '<p class="pushup ThisMonth">'. $analytics->total_monthly_pushes->total_push_recipients .' This Month</p>';
  $output .= '<p class="pushup AllTime">'. $analytics->total_all_time_pushes->total_push_recipients  .' All Time</p>';
  $output .= '

      </div>
      <p class="description">Total Number of Notifications Sent to Subscribers</p>

      <script type="text/javascript">
        new Chart( jQuery( document.querySelector( "#pushup-chart-recipients" ) ).get(0).getContext( "2d" ) ).Pie( [
                  {
            color : "#63bde4",
            value : '. $analytics->total_monthly_pushes->total_push_recipients .'					},									{
            color : "rgba(74,198,143,1)",
            value : '. $analytics->total_all_time_pushes->total_push_recipients .'					}								], pushup_chart_options );
      </script>
    </div>

  </td></tr></tbody></table>';


   return $output;
 }

/**
 * Area for changing the notification display settings
 */
 function pushup_notifications_admin_form($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'pushup') .'/css/settings.css');
  drupal_add_js(drupal_get_path('module', 'pushup') .'/js/chart.js');
  $form['pushup_website_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Website Name'),
    '#default_value' => variable_get('pushup_website_name'),
    '#description' => t('The website name for your notification messages, typically the name of your website or your URL.'),
  );
  $form['pushup_post_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Post Title'),
    '#default_value' => variable_get('pushup_post_title','Just published...'),
    '#description' => t('Title text to help motivate your audience to click their notification. ("Just published..." is the default.)'),
  );
  $form['pushup_icon'] = array(
    '#type' => 'managed_file',
    '#title' => t('Icon(s)'),
    '#description' => t('For best results, upload a 256x256 .PNG image. (Transparency is allowed.)'),
    '#default_value' => variable_get('pushup_icon', ''),
    '#upload_location' => 'public://pushup_icon/',
    '#theme' => 'pushup_thumb_upload',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      // Pass the maximum file size in bytes - 10MB
      'file_validate_size' => array(10*1024*1024),
    ),
  );
  $form['#submit'][] = 'pushup_notifications_form_submit';


   return system_settings_form($form);
 }

/**
 * Returns HTML for a managed file element with thumbnail.
 */
function theme_pushup_thumb_upload($variables) {
  $element = $variables['element'];
  $output = '';
  $output = print_r($element['#file'],true);
  if (isset($element['#file']->uri)) {
        $output = '<div id="edit-logo-ajax-wrapper"><div class="form-item form-type-managed-file form-item-logo"><span class="file">';
        $output .= '<img height="50px" src="' . image_style_url('thumbnail', $element['#file']->uri) . '" />';
        $output .= '</span><input type="submit" id="edit-' . $element['#name'] . '-remove-button" name="' . $element['#name'] . '_remove_button" value="Remove" class="form-submit ajax-processed">';
        $output .= '<input type="hidden" name="' . $element['#name'] . '[fid]" value="' . $element['#file']->fid . '">';
        return $output;
  }
  return $output;
}

/*
 * Handles the form submit
 */
function pushup_notifications_form_submit($form, $form_state) {
  $file = file_load($form_state['values']['pushup_icon']);
  $file->status = FILE_STATUS_PERMANENT;
  file_usage_add($file,'pushup','user',1);
  file_save($file);
  $file_path = file_create_url($file->uri);
  pushup_update_push_package_icon($file_path);
  $website_name = $form_state['values']['pushup_website_name'];
  variable_set('pushup_website_name',$website_name);
  pushup_set_website_name($website_name);
  $post_title = $form_state['values']['pushup_post_title'];
  variable_set('pushup_post_title',$post_title);
}


/**
 * Pushup settings form.
 */
function pushup_admin_form($form, &$form_state) {

  // PushUp external APIs settings.
  $form['pushup'] = array(
    '#type' => 'fieldset',
    '#title' => t('PushUp Settings'),
    '#description' => t('The following settings connect PushUp module with external APIs. '),
  );
  $form['pushup']['pushup_username'] = array(
    '#type' => 'textfield',
    '#title' => t('PushUp Username'),
    '#default_value' => variable_get('pushup_username', ''),
  );
  $form['pushup']['pushup_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('PushUp API Key'),
    '#default_value' => variable_get('pushup_api_key', ''),
  );
  return system_settings_form($form);
}






/**
	 * Handles updating an icon via the JSON API.
	 */
function pushup_update_push_package_icon($file_path = '') {
		//drupal_set_message('in update push package icon, file path is '. $file_path);
    $white_listed_icon_ids = array( '16x16', '16x16@2x', '32x32', '32x32@2x', '128x128', '128x128@2x' );

		if($file_path == ''){
      return false;
    }

		$result = pushup_set_icon( $file_path, '128x128@2x', 'basic' );
	}


/*
 * Function to set the icon
 */
 function pushup_set_icon($file_path = '') {
   global $base_url;
   if ($file_path == '') {
     return false;
   }
   //echo '<p>In set icon, file path is '. $file_path;
   $data = pushup_get_base64_image( $file_path );
		if ( empty( $data ) ) {
			return false;
		}
		$result = pushup_perform_authenticated_query( array(
			'push.icons.update' => array(
				'domain' => $base_url,
				'data'   => $data,
				'id'     => '128x128@2x',
				'mode'   => 'basic',
			),
		) );

    if($result->{'push.icons.update'}) {
      drupal_set_message('Icon updated');
    } else {
      drupal_set_message('Icon not updated - an error occured','error');
    }
 }


/**
 * Sets this domain's website name via the JSON API.
 *
 * @param string $name
 * @return bool
 */
function pushup_set_website_name( $name = '', $username = '', $api_key = '' ) {
		global $base_url;
    $result = pushup_perform_authenticated_query( array(
			'push.domain.config.setWebsiteName' => array(
				'domain'       => $base_url,
				'website_name' => $name,
			),
		), $username, $api_key );

    if($result->{'push.domain.config.setWebsiteName'} == 1) {
      drupal_set_message('Website Name Updated');
    } else {
      drupal_set_message('Website Name could not be updated.','error');
    }


	}

 	/**
	 * Takes an image URL and fetches the contents of that image and then base64 encodes the image so it can be
	 * manipulated.
	 *
	 * @param string $url
	 * @return bool|string
	 */
	function pushup_get_base64_image( $url = '' ) {
		$args = array(
			'timeout' => 20,
			'sslverify' => false,
		);

		$result = drupal_http_request( $url, $args );
		if (empty( $result ) || ($result->code != 200)) {
			return false;
		}

		$binary_image = $result->data;
		if ( empty( $binary_image ) ) {
			return false;
		}

		return base64_encode( $binary_image );
	}
