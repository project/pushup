(function ($) {

/**
 * Update the summary for the pushup module's vertical tab.
 */
Drupal.behaviors.vertical_tabs_exampleFieldsetSummaries = {
  attach: function (context) {
    // Use the fieldset class to identify the vertical tab element
    $('fieldset#edit-pushup', context).drupalSetSummary(function (context) {
      // Depending on the checkbox status, the settings will be customized, so
      // update the summary with the custom setting textfield string or a use a
      // default string.
      if($('#edit-pushup-data',context)) {
        var $data = $('fieldset#edit-pushup #edit-pushup-data div.description').text();
        return $data;
      }
      if ($('#edit-pushup-enabled', context).attr('checked')) {
        return Drupal.t('Send Push Notification');
      } else {
        return Drupal.t('Do not send Push Notification');
      }

    });
  }
};

})(jQuery);
