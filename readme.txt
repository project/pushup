=== PushUp Notifications ===
Contributors: 10up, johneckman
Tags: push notifications, push notification, notifications, push, news, services
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Effortlessly and selectively deliver push notifications to your readers, instantly alerting them about new content.

== Description ==

PushUp effortlessly connects your Drupal site to our premium **push notification delivery service**, so that you can selectively deliver on-demand notifications to your readers as you update your content.

PushUp requires an account at [pushupnotifications.com](http://pushupnotifications.com), in order to use our high scale push delivery infrastructure.

[youtube http://www.youtube.com/watch?v=HWu4XWEesX4]

PushUp currently supports [Safari Push Notifications on OS X Mavericks](https://developer.apple.com/notifications/safari-push-notifications/); support for additional browsers and services is coming soon.

* **Real-time analytics.** Monitor engagement, as it happens. How many eligible visitors accepted or declined notifications? How many subscribers do you have?
* **Pay as your grow.** Pay for what you need, when you need it, so increased engagement never interrupts your service. No subscribers? No fee. Tightening your belt? Don't click push, don't pay PushUp.
* **Be selective with one-check push requests.** PushUp adds a "Send push notification" checkbox directly on your node edit form. Respect your readers - and wallet - by pushing your best content. 
* **Impatient writer syndrome protection.** No matter how many times your author mashes that publish button, the same notification will never be sent twice.
* **Built for scale, battle tested by big names.** Based on the WordPress plugin (and using the same back-end notification delivery infrastructure) already running like a champ on sites like [9to5mac.com](http://9to5mac.com), the world's most popular Apple news blog,  [Deadline.com](http://deadline.com), and [Edelman.com](http://edelman.com). We've already delivered over 18 million notifications, including more than 500,000 notifications in a single day.
* **No extra software necessary.** We leverage technology built right into the browser and operating system, beginning with Safari on OS X Mavericks. Your readers simply accept a notification request - that only shows up the first time they visit - and they will start receiving notifications. Even when their browser is closed.

== Installation ==

1. Upload the module to your site's module directory
1. Activate the module through the Drupal admin
1. Follow the instructions to setup or activate your [pushupnotifications.com](http://pushupnotifications.com) account.

== Frequently Asked Questions ==

Find a full list of [FAQs on our website](https://pushupnotifications.com/faq/).

= How do my website visitors unsubscribe to notifications? =

Visitors can unsubscribe by opening Safari preferences from the "Safari" menu, clicking on the "Notifications" icon, finding your website in the list of sites, and choosing "Deny" (instead of "Allow").

= Why do you only support Safari 7 on OS X Mavericks (and newer)? =

We think Apple's implementation of website push notifications is the best we've seen - the browser doesn't have to be open, and the implementation uses absolutely no additional system resources. We already have plans to extend the service beyond Safari 7.

= What kind of analytics do you provide? =

At the moment, we provide the following data points:

1.	Conversion rate: number of potential subscribers who accepted notifications.
1.	Total number of current subscribers.
1.	Total number of unique push notifications sent this month.
1.	Total number of unique push notifications sent (all time).

We plan to implement notification delivery click-through (how many visitors actually clicked back to your site from a notification) in a soon to come update.

= How much does PushUp cost? =

We charge a one time site setup fee (typically $14.99) for your first site, which provides up to 100 unique notifications each month for life, as well unlimited use for the first 30 days.

After the first month, we have a "pay as you grow" model where you pay for the actual volume of unique notifications sent that month, with tiered discounts for higher volume. Small businesses shouldn't expect to pay more than $1.99 or $4.99 each month. Even 1 million unique notifications will cost only $99.99. If you want to stop paying, just deactivate the plugin or don't check the push option. [You can play with the price calculator on our website.](https://pushupnotifications.com/#pricing-explore)


== Changelog ==

= 1.0 = 
First version, based on WordPress plugin and existing infrastructure